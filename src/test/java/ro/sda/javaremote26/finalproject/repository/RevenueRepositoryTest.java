package ro.sda.javaremote26.finalproject.repository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import ro.sda.javaremote26.finalproject.entities.Revenue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RevenueRepositoryTest {

    @Autowired
    RevenueRepository revenueRepository;

    private Map<Integer, Revenue> testRevenue = new HashMap<>();

    @BeforeEach
    void setUp() {
        revenueRepository.deleteAll();

        Revenue revenue;
        int testId = 1;
        revenue = Revenue.builder().amount(100 + testId).build();
        revenueRepository.save(revenue);
        testRevenue.put(testId++, revenue);
        revenue = Revenue.builder().amount(100 + testId).build();
        revenueRepository.save(revenue);
        testRevenue.put(testId++, revenue);
        revenue = Revenue.builder().amount(100 + testId).build();
        revenueRepository.save(revenue);
        testRevenue.put(testId++, revenue);
        revenue = Revenue.builder().amount(100 + testId).build();
        revenueRepository.save(revenue);
        testRevenue.put(testId++, revenue);
        revenue = Revenue.builder().amount(100 + testId).build();
        revenueRepository.save(revenue);
        testRevenue.put(testId++, revenue);
    }

    @Test
    void findAll() {
        List<Revenue> revenueList = revenueRepository.findAll();
        Assertions.assertEquals(testRevenue.size(), revenueList.size(), "All list is expected to be returned");
    }
}