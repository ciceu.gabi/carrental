package ro.sda.javaremote26.finalproject.repository;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ro.sda.javaremote26.finalproject.builder.BranchBuilder;
import ro.sda.javaremote26.finalproject.builder.CarBuilder;
import ro.sda.javaremote26.finalproject.builder.EmployeeBuilder;
import ro.sda.javaremote26.finalproject.builder.ReservationBuilder;
import ro.sda.javaremote26.finalproject.entities.*;
import ro.sda.javaremote26.finalproject.enums.Position;

import javax.sql.DataSource;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

//@DataJpaTest
//@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles("test")
class BranchRepositoryTest {

    @Autowired
    private DataSource dataSource;
    @Autowired
    private BranchRepository branchRepository;
    @Autowired
    private CarRepository carRepository;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private ReservationRepository reservationRepository;

    private Car car;
    private Branch branch;
    private Employee employee;
    private Reservation reservation;

    @BeforeEach
    void setUp() {
        this.branch = BranchBuilder.aBranch().withAddress("Some address").build();
        this.car = CarBuilder.aCar().withColor("Red").build();
        this.employee = EmployeeBuilder.anEmployee().withFirstName("Aurel").withName("Popa").withPosition(Position.MANAGER).build();
        this.reservation = ReservationBuilder.aReservation().withCar(car).withAmount(200).withBranchOfLoan(branch).build();
        carRepository.save(car);
        employeeRepository.save(employee);
        branchRepository.save(branch);
        reservationRepository.save(reservation);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void injectedComponentsAreNotNull() {
        assertNotNull(dataSource);
        assertNotNull(branchRepository);
        assertNotNull(carRepository);
        assertNotNull(employeeRepository);
    }

    @Test
    void createCar() {
        assertNotNull(car.getId(), "Car was not created");
    }

    @Test
    void createEmployee() {
        assertNotNull(employee.getId(), "Employee was not created");
    }

    @Test
    void creatBranch() {
        assertNotNull(branch.getId(), "Branch failed to be created");
    }

    @Test
    void createReservation() {
        assertNotNull(reservation.getId(), "Reservation failed to be created");
    }

    @Test
    void createEntityAndLinkBetweenBranchAndCar() {
        Long timestamp = System.currentTimeMillis();
        this.branch.setAddress(branch.getAddress() + timestamp);
        branchRepository.save(branch);
        this.car.setBranch(branch);
        carRepository.save(car);

    }

    @Test
    void createEntityAndLinkBetweenBranchAndEmployee() {
        this.branch.setAddress(branch.getAddress());
        branchRepository.save(branch);
        this.employee.setBranch(branch);
        employeeRepository.save(employee);
    }

    @Test
    void createEntityAndLinkBetweenReservationAndBranch() {
        this.branch.setAddress(branch.getAddress());
        branchRepository.save(branch);
        this.reservation.setBranchOfLoan(branch);
        reservationRepository.save(reservation);
    }
}