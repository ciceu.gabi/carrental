package ro.sda.javaremote26.finalproject.repository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import ro.sda.javaremote26.finalproject.entities.Car;
import ro.sda.javaremote26.finalproject.entities.Reservation;
import ro.sda.javaremote26.finalproject.enums.Brand;

import java.text.DateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ReservationRepositoryTest {

    @Autowired
    ReservationRepository reservationRepository;

    private Map<Integer, Reservation> testReservation = new HashMap<>();

    @BeforeEach
    void setUp() {
        reservationRepository.deleteAll();

        Reservation reservation;
        int testId = 1;
        reservation = Reservation.builder().amount(500 + testId).dateFrom(Date.from(Instant.now())).build();
        reservationRepository.save(reservation);
        testReservation.put(testId++, reservation);
        reservation = Reservation.builder().amount(500 + testId).dateFrom(Date.from(Instant.now())).build();
        reservationRepository.save(reservation);
        testReservation.put(testId++, reservation);
        reservation = Reservation.builder().amount(500 + testId).dateFrom(Date.from(Instant.now())).build();
        reservationRepository.save(reservation);
        testReservation.put(testId++, reservation);
        reservation = Reservation.builder().amount(500 + testId).dateFrom(Date.from(Instant.now())).build();
        reservationRepository.save(reservation);
        testReservation.put(testId++, reservation);
        reservation = Reservation.builder().amount(500 + testId).dateFrom(Date.from(Instant.now())).build();
        reservationRepository.save(reservation);
        testReservation.put(testId++, reservation);
    }
    @Test
    void findAll() {
        List<Reservation> reservationList = reservationRepository.findAll();
        Assertions.assertEquals(testReservation.size(), reservationList.size(), "All list is expected to be returned");
    }
}