package ro.sda.javaremote26.finalproject.repository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import ro.sda.javaremote26.finalproject.entities.Car;
import ro.sda.javaremote26.finalproject.entities.Loan;
import ro.sda.javaremote26.finalproject.enums.Brand;

import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class LoanRepositoryTest {

    @Autowired
    LoanRepository loanRepository;

    private Map<Integer, Loan> testLoan = new HashMap<>();

    @BeforeEach
    void setUp() {
        loanRepository.deleteAll();

        Loan loan;
        int testId = 1;
        loan = Loan.builder().comments("comments_"+ testId).dateOfRental(Date.from(Instant.now())).build();
        loanRepository.save(loan);
        testLoan.put(testId++, loan); loan = Loan.builder().comments("comments_"+ testId).dateOfRental(Date.from(Instant.now())).build();
        loanRepository.save(loan);
        testLoan.put(testId++, loan); loan = Loan.builder().comments("comments_"+ testId).dateOfRental(Date.from(Instant.now())).build();
        loanRepository.save(loan);
        testLoan.put(testId++, loan); loan = Loan.builder().comments("comments_"+ testId).dateOfRental(Date.from(Instant.now())).build();
        loanRepository.save(loan);
        testLoan.put(testId++, loan); loan = Loan.builder().comments("comments_"+ testId).dateOfRental(Date.from(Instant.now())).build();
        loanRepository.save(loan);
        testLoan.put(testId++, loan);
    }
    @Test
    void findAll() {
        List<Loan> loanList = loanRepository.findAll();
        Assertions.assertEquals(testLoan.size(), loanList.size(), "All list is expected to be returned");
    }
}