package ro.sda.javaremote26.finalproject.repository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import ro.sda.javaremote26.finalproject.entities.Car;
import ro.sda.javaremote26.finalproject.entities.Refund;
import ro.sda.javaremote26.finalproject.enums.Brand;

import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RefundRepositoryTest {

    @Autowired
    RefundRepository refundRepository;

    private Map<Integer, Refund> testRefund = new HashMap<>();

    @BeforeEach
    void setUp() {

        refundRepository.deleteAll();

        Refund refund;
        int testId = 1;
        refund = Refund.builder().comments("comments_" + testId).dateOfReturn(Date.from(Instant.now())).surcharge(200).build();
        refundRepository.save(refund);
        testRefund.put(testId++, refund);
        refund = Refund.builder().comments("comments_" + testId).dateOfReturn(Date.from(Instant.now())).surcharge(200).build();
        refundRepository.save(refund);
        testRefund.put(testId++, refund);
        refund = Refund.builder().comments("comments_" + testId).dateOfReturn(Date.from(Instant.now())).surcharge(200).build();
        refundRepository.save(refund);
        testRefund.put(testId++, refund);
        refund = Refund.builder().comments("comments_" + testId).dateOfReturn(Date.from(Instant.now())).surcharge(200).build();
        refundRepository.save(refund);
        testRefund.put(testId++, refund);
        refund = Refund.builder().comments("comments_" + testId).dateOfReturn(Date.from(Instant.now())).surcharge(200).build();
        refundRepository.save(refund);
        testRefund.put(testId++, refund);
    }
    @Test
    void findAll() {
        List<Refund> refundList = refundRepository.findAll();
        Assertions.assertEquals(testRefund.size(), refundList.size(), "All list is expected to be returned");
    }
}