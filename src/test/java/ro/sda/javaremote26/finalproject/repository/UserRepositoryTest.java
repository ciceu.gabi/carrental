package ro.sda.javaremote26.finalproject.repository;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import ro.sda.javaremote26.finalproject.entities.User;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserRepositoryTest {

    @Autowired
    private UserRepository repository;

    private Map<Integer, User> testUsers = new HashMap<>();

    @BeforeAll
    private void setUp() {
        //clean repo
        repository.deleteAll();

        User user;
        int testId = 1;
        user = new User(null, "Grigore_" + testId, "Grigorescu", "user_" + testId + "@mail", "adres_" + testId, "{encrypted}pwd", "RO", null);
        repository.save(user);
        testUsers.put(testId++, user);

        user = new User(null, "Grigore_" + testId, "Grigorescu", "user_" + testId + "@mail", "adres_" + testId, "{encrypted}pwd", "RO", null);
        repository.save(user);
        testUsers.put(testId++, user);

        user = new User(null, "Grigore_" + testId, "Grigorescu", "user_" + testId + "@mail", "adres_" + testId, "{encrypted}pwd", "RO", null);
        repository.save(user);
        testUsers.put(testId++, user);

        user = new User(null, "Grigore_" + testId, "Grigorescu", "user_" + testId + "@mail", "adres_" + testId, "{encrypted}pwd", "RO", null);
        repository.save(user);
        testUsers.put(testId++, user);

        user = new User(null, "Grigore_" + testId, "Grigorescu", "user_" + testId + "@mail", "adres_" + testId, "{encrypted}pwd", "RO", null);
        repository.save(user);
        testUsers.put(testId++, user);

        user = new User(null, "Grigore_" + testId, "Grigorescu", "user_" + testId + "@mail", "adres_" + testId, "{encrypted}pwd", "RO", null);
        repository.save(user);
        testUsers.put(testId++, user);

    }

    @Test
    void findByName() {
        List<User> oneUser = repository.findByName("Grigore_2");
        assertEquals(1, oneUser.size(), "Only one user is expected to be returned");
        User actual = oneUser.get(0);
        User expected = testUsers.get(2);

        assertEquals(expected.getId(), actual.getId(), "User id doesn't match");
        assertEquals(expected.getAddress(), actual.getAddress(), "User address doesn't match");
        assertEquals(expected.getName(), actual.getName(), "User name doesn't match");
        assertEquals(expected.getFirstName(), actual.getFirstName(), "User first name doesn't match");
        assertEquals(expected.getCountry(), actual.getCountry(), "User country doesn't match");
        assertEquals(expected.getEmail(), actual.getEmail(), "User email doesn't match");
        assertEquals(expected.getPassword(), actual.getPassword(), "User password doesn't match");

    }

    @Test
    void findByEmail() {
        Optional<User> oneUser = repository.findByEmail("user_2@mail");
        assertEquals(false, oneUser.isEmpty(), "Optional should exist");
        User actual = oneUser.get();
        User expected = testUsers.get(2);

        assertEquals(expected.getId(), actual.getId(), "User id doesn't match");
        assertEquals(expected.getAddress(), actual.getAddress(), "User address doesn't match");
        assertEquals(expected.getName(), actual.getName(), "User name doesn't match");
        assertEquals(expected.getFirstName(), actual.getFirstName(), "User first name doesn't match");
        assertEquals(expected.getCountry(), actual.getCountry(), "User country doesn't match");
        assertEquals(expected.getEmail(), actual.getEmail(), "User email doesn't match");
        assertEquals(expected.getPassword(), actual.getPassword(), "User password doesn't match");
    }

    @Test
    void findAll() {
        List<User> userList = repository.findAll();
        assertEquals(testUsers.size(), userList.size(), "All List is expected to be returned");
    }


}