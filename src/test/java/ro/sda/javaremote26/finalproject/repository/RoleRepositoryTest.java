package ro.sda.javaremote26.finalproject.repository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import ro.sda.javaremote26.finalproject.entities.Car;
import ro.sda.javaremote26.finalproject.entities.Role;
import ro.sda.javaremote26.finalproject.enums.Brand;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RoleRepositoryTest {

    @Autowired
    RoleRepository roleRepository;

    private Map<Integer, Role> testRole = new HashMap<>();

    @BeforeEach
    void setUp() {
        roleRepository.deleteAll();

        Role role;
        int testId = 1;
        role = Role.builder().name("name_" + testId).build();
        roleRepository.save(role);
        testRole.put(testId++, role);
        role = Role.builder().name("name_" + testId).build();
        roleRepository.save(role);
        testRole.put(testId++, role);
        role = Role.builder().name("name_" + testId).build();
        roleRepository.save(role);
        testRole.put(testId++, role);
        role = Role.builder().name("name_" + testId).build();
        roleRepository.save(role);
        testRole.put(testId++, role);
        role = Role.builder().name("name_" + testId).build();
        roleRepository.save(role);
        testRole.put(testId++, role);
    }

    @Test
    void findAll() {
        List<Role> roleList = roleRepository.findAll();
        Assertions.assertEquals(testRole.size(), roleList.size(), "All list is expected to be returned");
    }

    @Test
    void findByName(){
        Optional<Role> oneRole = roleRepository.findByName("name_2");
        assertEquals(false, oneRole.isEmpty(), "Optional should exist");
        Role actual = oneRole.get();
        Role expected = testRole.get(2);

        assertEquals(expected.getName(), actual.getName(), "Role name doesn't match");
    }
}