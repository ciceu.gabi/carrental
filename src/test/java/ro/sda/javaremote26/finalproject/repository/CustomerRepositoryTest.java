package ro.sda.javaremote26.finalproject.repository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import ro.sda.javaremote26.finalproject.entities.Car;
import ro.sda.javaremote26.finalproject.entities.Customer;
import ro.sda.javaremote26.finalproject.enums.Brand;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CustomerRepositoryTest {

    @Autowired
    CustomerRepository customerRepository;

    private Map<Integer, Customer> testCustomer = new HashMap<>();

    @BeforeEach
    void setUp() {
        customerRepository.deleteAll();

        Customer customer;
        int testId = 1;
        customer = Customer.builder().address("Address_" + testId).email("Email").firstName("FName").name("Name").build();
        customerRepository.save(customer);
        testCustomer.put(testId++, customer);
        customer = Customer.builder().address("Address_" + testId).email("Email").firstName("FName").name("Name").build();
        customerRepository.save(customer);
        testCustomer.put(testId++, customer);
        customer = Customer.builder().address("Address_" + testId).email("Email").firstName("FName").name("Name").build();
        customerRepository.save(customer);
        testCustomer.put(testId++, customer);
        customer = Customer.builder().address("Address_" + testId).email("Email").firstName("FName").name("Name").build();
        customerRepository.save(customer);
        testCustomer.put(testId++, customer);
        customer = Customer.builder().address("Address_" + testId).email("Email").firstName("FName").name("Name").build();
        customerRepository.save(customer);
        testCustomer.put(testId++, customer);
    }

    @Test
    void findAll() {
        List<Customer> customerList = customerRepository.findAll();
        Assertions.assertEquals(testCustomer.size(), customerList.size(), "All list is expected to be returned");
    }
}