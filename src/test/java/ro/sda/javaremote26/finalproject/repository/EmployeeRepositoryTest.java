package ro.sda.javaremote26.finalproject.repository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import ro.sda.javaremote26.finalproject.entities.Branch;
import ro.sda.javaremote26.finalproject.entities.Car;
import ro.sda.javaremote26.finalproject.entities.Employee;
import ro.sda.javaremote26.finalproject.enums.Brand;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class EmployeeRepositoryTest {

    @Autowired
    EmployeeRepository employeeRepository;

    private Map<Integer, Employee> testEmployee = new HashMap<>();

    @BeforeEach
    void setUp() {
        employeeRepository.deleteAll();

        Employee employee;
        int testId = 1;
        employee = Employee.builder().firstName("FName_" + testId).name("Name").build();
        employeeRepository.save(employee);
        testEmployee.put(testId++, employee);
        employee = Employee.builder().firstName("FName_" + testId).name("Name").build();
        employeeRepository.save(employee);
        testEmployee.put(testId++, employee);
        employee = Employee.builder().firstName("FName_" + testId).name("Name").build();
        employeeRepository.save(employee);
        testEmployee.put(testId++, employee);
        employee = Employee.builder().firstName("FName_" + testId).name("Name").build();
        employeeRepository.save(employee);
        testEmployee.put(testId++, employee);
        employee = Employee.builder().firstName("FName_" + testId).name("Name").build();
        employeeRepository.save(employee);
        testEmployee.put(testId++, employee);
        employee = Employee.builder().firstName("FName_" + testId).name("Name").build();
        employeeRepository.save(employee);
        testEmployee.put(testId++, employee);
    }

    @Test
    void findAll() {
        List<Employee> employeeList = employeeRepository.findAll();
        Assertions.assertEquals(testEmployee.size(), employeeList.size(), "All list is expected to be returned");
    }
}