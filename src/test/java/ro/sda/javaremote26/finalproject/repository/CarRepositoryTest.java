package ro.sda.javaremote26.finalproject.repository;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import ro.sda.javaremote26.finalproject.entities.Car;
import ro.sda.javaremote26.finalproject.enums.Brand;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CarRepositoryTest {

    @Autowired
    CarRepository carRepository;

    private Map<Integer, Car> testCar = new HashMap<>();

    @BeforeEach
    private void setUp() {
        carRepository.deleteAll();

        Car car;
        int testId = 1;
        car = Car.builder().brand(Brand.AUDI).year(2021).bodyType("Berlin_" + testId).color("Red").model("A8").build();
        carRepository.save(car);
        testCar.put(testId++, car);
        car = Car.builder().brand(Brand.LAMBORGHINI).year(2022).bodyType("Hatchback_" + testId).color("Blue").model("A3").build();
        carRepository.save(car);
        testCar.put(testId++, car);
        car = Car.builder().brand(Brand.RENAULT).year(2020).bodyType("Coupe_" + testId).color("Black").model("CX").build();
        carRepository.save(car);
        testCar.put(testId++, car);
        car = Car.builder().brand(Brand.BMW).year(2019).bodyType("Sport_" + testId).color("Green").model("X5").build();
        carRepository.save(car);
        testCar.put(testId++, car);
        car = Car.builder().brand(Brand.NISSAN).year(2017).bodyType("OffRoad_" + testId).color("Purple").model("Patrol").build();
        carRepository.save(car);
        testCar.put(testId++, car);
    }

    @Test
    void findAll() {
        List<Car> carList = carRepository.findAll();
        Assertions.assertEquals(testCar.size(), carList.size(), "All list is expected to be returned");
    }

    @Test
    void findAllByBrandAndBodyTypeAndModelAndYear() {
        List<Car> oneCar = carRepository.findAllByBrandAndBodyTypeAndModelAndYear(Brand.LAMBORGHINI,"Hatchback_2", "A3", 2022);
        assertEquals(1, oneCar.size(), "Only one car is expected to be returned");
        Car actual = oneCar.get(0);
        Car expected = testCar.get(2);

        assertEquals(expected.getId(), actual.getId(), "Car id doesn't match");
        assertEquals(expected.getBodyType(), actual.getBodyType(), "Car body type doesn't match");
        assertEquals(expected.getBrand(), actual.getBrand(), "Car brand doesn't match");
        assertEquals(expected.getBranch(), actual.getBranch(), "Car branch doesn't match");
        assertEquals(expected.getModel(), actual.getModel(), "Car model doesn't match");
        assertEquals(expected.getAmountPerDay(), actual.getAmountPerDay(), "Car amount doesn't match");
        assertEquals(expected.getColor(), actual.getColor(), "Car color doesn't match");
        assertEquals(expected.getDescription(), actual.getDescription(), "Car description doesn't match");
        assertEquals(expected.getKilometers(), actual.getKilometers(), "Car kilometers doesn't match");
        assertEquals(expected.getStatus(), actual.getStatus(), "Car status doesn't match");
        assertEquals(expected.getYear(), actual.getYear(), "Car year doesn't match");


    }
}