package ro.sda.javaremote26.finalproject.repository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import ro.sda.javaremote26.finalproject.entities.Car;
import ro.sda.javaremote26.finalproject.entities.Rental;
import ro.sda.javaremote26.finalproject.enums.Brand;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RentalRepositoryTest {

    @Autowired
    RentalRepository rentalRepository;

    private Map<Integer, Rental> testRental = new HashMap<>();

    @BeforeEach
    void setUp() {
        rentalRepository.deleteAll();

        Rental rental;
        int testId = 1;
        rental = Rental.builder().contactAddress("address_" + testId).internetDomain("domain").logoType("logo").build();
        rentalRepository.save(rental);
        testRental.put(testId++, rental);
        rental = Rental.builder().contactAddress("address_" + testId).internetDomain("domain").logoType("logo").build();
        rentalRepository.save(rental);
        testRental.put(testId++, rental);
        rental = Rental.builder().contactAddress("address_" + testId).internetDomain("domain").logoType("logo").build();
        rentalRepository.save(rental);
        testRental.put(testId++, rental);
        rental = Rental.builder().contactAddress("address_" + testId).internetDomain("domain").logoType("logo").build();
        rentalRepository.save(rental);
        testRental.put(testId++, rental);
        rental = Rental.builder().contactAddress("address_" + testId).internetDomain("domain").logoType("logo").build();
        rentalRepository.save(rental);
        testRental.put(testId++, rental);
    }
    @Test
    void findAll() {
        List<Rental> rentalList = rentalRepository.findAll();
        Assertions.assertEquals(testRental.size(), rentalList.size(), "All list is expected to be returned");
    }
}