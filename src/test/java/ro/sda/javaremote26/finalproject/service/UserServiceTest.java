package ro.sda.javaremote26.finalproject.service;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import ro.sda.javaremote26.finalproject.dto.UserDto;
import ro.sda.javaremote26.finalproject.entities.Role;
import ro.sda.javaremote26.finalproject.entities.User;
import ro.sda.javaremote26.finalproject.mapper.UserMapper;
import ro.sda.javaremote26.finalproject.repository.RoleRepository;
import ro.sda.javaremote26.finalproject.repository.UserRepository;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@SpringBootTest
class UserServiceTest {

    @InjectMocks
    UserService userService;

    @Mock
    private UserMapper userMapper;

    @Mock
    private UserRepository userRepository;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @DisplayName("Test new admin user registration")
    @Test
    void registerAdminUser() {
        User mockUser = mock(User.class);
        when(mockUser.getPassword()).thenReturn("password");
        UserDto userDtoMock = mock(UserDto.class);
        when(userMapper.map(any(UserDto.class))).thenReturn(mockUser);
        when(userDtoMock.isAdmin()).thenReturn(true);
        Role mockRole = mock(Role.class);
        when(roleRepository.findByName(anyString())).thenReturn(Optional.of(mockRole));
        // user set role
        when(bCryptPasswordEncoder.encode(anyString())).thenReturn("123");

        // method under test
        userService.register(userDtoMock);

        //verify that map is called once and that it is called with our userDtoMock supplied
        ArgumentCaptor<UserDto> dtoCaptor = ArgumentCaptor.forClass(UserDto.class);
        verify(userMapper, times(1)).map(dtoCaptor.capture());
        assertEquals(userDtoMock, dtoCaptor.getValue());

        // verify logic from assignRolesToUser
        ArgumentCaptor<String> roleCaptor = ArgumentCaptor.forClass(String.class);
        verify(roleRepository, times(1)).findByName(roleCaptor.capture());
        assertEquals("ROLE_ADMIN", roleCaptor.getValue());

        //verify encoder is called
        ArgumentCaptor<String> passwordCaptor = ArgumentCaptor.forClass(String.class);
        verify(bCryptPasswordEncoder, times(1)).encode(passwordCaptor.capture());
        assertEquals("password", passwordCaptor.getValue());

        //verify mocking works
        ArgumentCaptor<String> setPasswordCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockUser, times(1)).setPassword(setPasswordCaptor.capture());
        assertEquals("123", setPasswordCaptor.getValue());

    }

    @DisplayName("Test new user registration")
    @Test
    void registerRegularUser() {
        User mockUser = mock(User.class);
        when(mockUser.getPassword()).thenReturn("password");
        UserDto userDtoMock = mock(UserDto.class);
        when(userMapper.map(any(UserDto.class))).thenReturn(mockUser);
        when(userDtoMock.isAdmin()).thenReturn(false);
        Role mockRole = mock(Role.class);
        when(roleRepository.findByName(anyString())).thenReturn(Optional.of(mockRole));
        // user set role
        when(bCryptPasswordEncoder.encode(anyString())).thenReturn("123");

        // method under test
        userService.register(userDtoMock);

        //verify that map is called once and that it is called with our userDtoMock supplied
        ArgumentCaptor<UserDto> dtoCaptor = ArgumentCaptor.forClass(UserDto.class);
        verify(userMapper, times(1)).map(dtoCaptor.capture());
        assertEquals(userDtoMock, dtoCaptor.getValue());

        // verify logic from assignRolesToUser
        ArgumentCaptor<String> roleCaptor = ArgumentCaptor.forClass(String.class);
        verify(roleRepository, times(1)).findByName(roleCaptor.capture());
        assertEquals("ROLE_USER", roleCaptor.getValue());

        //verify encoder is called
        ArgumentCaptor<String> passwordCaptor = ArgumentCaptor.forClass(String.class);
        verify(bCryptPasswordEncoder, times(1)).encode(passwordCaptor.capture());
        assertEquals("password", passwordCaptor.getValue());

        //verify mocking works
        ArgumentCaptor<String> setPasswordCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockUser, times(1)).setPassword(setPasswordCaptor.capture());
        assertEquals("123", setPasswordCaptor.getValue());
    }

    @DisplayName("Test update user")
    @Test
    void updateUSer() {
        User mockUser = mock(User.class);
        when(mockUser.getPassword()).thenReturn("password");
        UserDto userDtoMock = mock(UserDto.class);
        when(userMapper.map(any(UserDto.class))).thenReturn(mockUser);
        // user set role
        when(bCryptPasswordEncoder.encode(anyString())).thenReturn("123");

        // method under test
        userService.updateUser(userDtoMock);

        //verify that map is called once and that it is called with our userDtoMock supplied
        ArgumentCaptor<UserDto> dtoCaptor = ArgumentCaptor.forClass(UserDto.class);
        verify(userMapper, times(1)).map(dtoCaptor.capture());
        assertEquals(userDtoMock, dtoCaptor.getValue());

        //verify encoder is called
        ArgumentCaptor<String> passwordCaptor = ArgumentCaptor.forClass(String.class);
        verify(bCryptPasswordEncoder, times(1)).encode(passwordCaptor.capture());
        assertEquals("password", passwordCaptor.getValue());

        //verify mocking works
        ArgumentCaptor<String> setPasswordCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockUser, times(1)).setPassword(setPasswordCaptor.capture());
        assertEquals("123", setPasswordCaptor.getValue());
    }

}