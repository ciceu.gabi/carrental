package ro.sda.javaremote26.finalproject.entities;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.GenerationType.AUTO;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Reservation {
    @Id
    @GeneratedValue(strategy = AUTO)
    private Integer id;
    private Date dateOfBooking;
    @ManyToOne
    private User client;
    @ManyToOne
    private Car car;
    private Date dateFrom;
    private Date dateTo;
    @ManyToOne
    private Branch branchOfLoan;
    @ManyToOne
    private Branch returnDepartment;
    private int amount;

    @Override
    public String toString() {
        return client.toString();
    }
}
