package ro.sda.javaremote26.finalproject.entities;

import lombok.*;
import ro.sda.javaremote26.finalproject.enums.Position;

import javax.persistence.*;

import static javax.persistence.GenerationType.AUTO;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Employee {

    @Id
    @GeneratedValue(strategy = AUTO)
    private Integer id;
    private String firstName;
    private String name;
    private Position position;
    @ManyToOne(fetch = FetchType.EAGER)
    private Branch branch;
    @Lob
    private String image;

    @Override
    public String toString() {
        return firstName + " " + name;
    }
}
