package ro.sda.javaremote26.finalproject.entities;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.GenerationType.AUTO;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Loan {
    @Id
    @GeneratedValue(strategy = AUTO)
    private Integer id;
    @ManyToOne
    private Employee employee;
    private Date dateOfRental;
    @OneToOne
    private Reservation reservation;
    private String comments;
}
