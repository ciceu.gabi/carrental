package ro.sda.javaremote26.finalproject.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotBlank(message = "Name is mandatory")
    private String name;
    @NotBlank(message = "First Name is mandatory")
    private String firstName;
    @NotBlank(message = "Email is mandatory")
    @Column(unique = true)
    private String email;
    @NotBlank(message = "Address is mandatory")
    private String address;
    @NotBlank(message = "Password is mandatory")
    private String password;
    @NotBlank(message = "Country is mandatory")
    private String country;


    @ManyToOne(cascade = CascadeType.ALL)
    private Role role;
}
