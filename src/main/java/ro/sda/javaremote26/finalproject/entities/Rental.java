package ro.sda.javaremote26.finalproject.entities;

import lombok.*;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.GenerationType.AUTO;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Rental {
    @Id
    @GeneratedValue(strategy = AUTO)
    private Integer id;
    private String name;
    private String internetDomain;
    private String contactAddress;
    @ManyToOne
    private Customer owner;
    private String logoType;
    @OneToMany (mappedBy = "rental", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Branch> listOfBranches;

    @Override
    public String toString() {
        return contactAddress.toString();
    }
}
