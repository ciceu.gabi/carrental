package ro.sda.javaremote26.finalproject.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.AUTO;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Branch {
    @Id
    @GeneratedValue(strategy = AUTO)
    private Integer id;
    private String address;
    private String branchName;
    @OneToMany(mappedBy = "branch",fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Employee> facilityEmployees;
    @OneToMany(mappedBy = "branch",fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Car> availableCars;
    @ManyToOne
    private Rental rental;

    public void addCar(Car car){
        if (this.availableCars == null){
            this.availableCars = new ArrayList<>();
        }
        this.availableCars.add(car);
    }

    @Override
    public String toString() {
        return branchName;
    }
}
