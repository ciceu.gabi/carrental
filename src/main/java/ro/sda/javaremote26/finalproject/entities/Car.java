package ro.sda.javaremote26.finalproject.entities;

import lombok.*;
import org.springframework.lang.Nullable;
import ro.sda.javaremote26.finalproject.enums.Brand;
import ro.sda.javaremote26.finalproject.enums.Status;

import javax.persistence.*;

import java.io.File;

import static javax.persistence.GenerationType.AUTO;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Car {
    @Id
    @GeneratedValue(strategy = AUTO)
    private Integer id;
    @Enumerated(EnumType.STRING)
    private Brand brand;
    private String model;
    private String bodyType;
    @Nullable
    private int year;
    private String color;
    private double kilometers;
    @Enumerated(EnumType.STRING)
    private Status status;
    private int amountPerDay;
    @ManyToOne
    private Branch branch;
    private String description;
    @Lob
    private String image;
    @Override
    public String toString() {
        return brand + " " + model;
    }
}
