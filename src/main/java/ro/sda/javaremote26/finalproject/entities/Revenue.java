package ro.sda.javaremote26.finalproject.entities;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import static javax.persistence.GenerationType.AUTO;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Revenue {
    @Id
    @GeneratedValue(strategy = AUTO)
    private Integer id;

    private int amount;
}
