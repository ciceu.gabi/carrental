package ro.sda.javaremote26.finalproject.builder;

import ro.sda.javaremote26.finalproject.entities.Branch;
import ro.sda.javaremote26.finalproject.entities.Car;
import ro.sda.javaremote26.finalproject.enums.Brand;
import ro.sda.javaremote26.finalproject.enums.Status;

public final class CarBuilder {
    private Integer id;
    private Brand brand;
    private String model;
    private String bodyType;
    private int year;
    private String color;
    private double kilometers;
    private Status status;
    private int amountPerDay;
    private Branch branch;

    private CarBuilder() {
    }

    public static CarBuilder aCar() {
        return new CarBuilder();
    }

    public CarBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    public CarBuilder withBrand(Brand brand) {
        this.brand = brand;
        return this;
    }

    public CarBuilder withModel(String model) {
        this.model = model;
        return this;
    }

    public CarBuilder withBodyType(String bodyType) {
        this.bodyType = bodyType;
        return this;
    }

    public CarBuilder withYear(int year) {
        this.year = year;
        return this;
    }

    public CarBuilder withColor(String color) {
        this.color = color;
        return this;
    }

    public CarBuilder withKilometers(double kilometers) {
        this.kilometers = kilometers;
        return this;
    }

    public CarBuilder withStatus(Status status) {
        this.status = status;
        return this;
    }

    public CarBuilder withAmountPerDay(int amountPerDay) {
        this.amountPerDay = amountPerDay;
        return this;
    }

    public CarBuilder withBranch(Branch branch) {
        this.branch = branch;
        return this;
    }

    public Car build() {
        Car car = new Car();
        car.setId(id);
        car.setBrand(brand);
        car.setModel(model);
        car.setBodyType(bodyType);
        car.setYear(year);
        car.setColor(color);
        car.setKilometers(kilometers);
        car.setStatus(status);
        car.setAmountPerDay(amountPerDay);
        car.setBranch(branch);
        return car;
    }
}
