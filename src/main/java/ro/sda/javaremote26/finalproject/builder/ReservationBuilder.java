package ro.sda.javaremote26.finalproject.builder;

import ro.sda.javaremote26.finalproject.entities.*;

import java.util.Date;

public final class ReservationBuilder {
    private Integer id;
    private Date dateOfBooking;
    private User client;
    private Car car;
    private Date dateFrom;
    private Date dateTo;
    private Branch branchOfLoan;
    private Branch returnDepartment;
    private int amount;

    private ReservationBuilder() {
    }

    public static ReservationBuilder aReservation() {
        return new ReservationBuilder();
    }

    public ReservationBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    public ReservationBuilder withDateOfBooking(Date dateOfBooking) {
        this.dateOfBooking = dateOfBooking;
        return this;
    }

    public ReservationBuilder withClient(User client) {
        this.client = client;
        return this;
    }

    public ReservationBuilder withCar(Car car) {
        this.car = car;
        return this;
    }

    public ReservationBuilder withDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
        return this;
    }

    public ReservationBuilder withDateTo(Date dateTo) {
        this.dateTo = dateTo;
        return this;
    }

    public ReservationBuilder withBranchOfLoan(Branch branchOfLoan) {
        this.branchOfLoan = branchOfLoan;
        return this;
    }

    public ReservationBuilder withReturnDepartment(Branch returnDepartment) {
        this.returnDepartment = returnDepartment;
        return this;
    }

    public ReservationBuilder withAmount(int amount) {
        this.amount = amount;
        return this;
    }

    public Reservation build() {
        Reservation reservation = new Reservation();
        reservation.setId(id);
        reservation.setDateOfBooking(dateOfBooking);
        reservation.setClient(client);
        reservation.setCar(car);
        reservation.setDateFrom(dateFrom);
        reservation.setDateTo(dateTo);
        reservation.setBranchOfLoan(branchOfLoan);
        reservation.setReturnDepartment(returnDepartment);
        reservation.setAmount(amount);
        return reservation;
    }
}
