package ro.sda.javaremote26.finalproject.builder;

import ro.sda.javaremote26.finalproject.entities.Branch;
import ro.sda.javaremote26.finalproject.entities.Employee;
import ro.sda.javaremote26.finalproject.enums.Position;

public final class EmployeeBuilder {
    private Integer id;
    private String firstName;
    private String name;
    private Position position;
    private Branch branch;

    private EmployeeBuilder() {
    }

    public static EmployeeBuilder anEmployee() {
        return new EmployeeBuilder();
    }

    public EmployeeBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    public EmployeeBuilder withFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public EmployeeBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public EmployeeBuilder withPosition(Position position) {
        this.position = position;
        return this;
    }

    public EmployeeBuilder withBranch(Branch branch) {
        this.branch = branch;
        return this;
    }

    public Employee build() {
        Employee employee = new Employee();
        employee.setId(id);
        employee.setFirstName(firstName);
        employee.setName(name);
        employee.setPosition(position);
        employee.setBranch(branch);
        return employee;
    }
}
