package ro.sda.javaremote26.finalproject.builder;

import ro.sda.javaremote26.finalproject.entities.Branch;
import ro.sda.javaremote26.finalproject.entities.Car;
import ro.sda.javaremote26.finalproject.entities.Employee;
import ro.sda.javaremote26.finalproject.entities.Rental;

import java.util.List;

public final class BranchBuilder {
    private Integer id;
    private String address;
    private String branchName;
    private List<Employee> facilityEmployees;
    private List<Car> availableCars;
    private Rental rental;

    private BranchBuilder() {
    }

    public static BranchBuilder aBranch() {
        return new BranchBuilder();
    }

    public BranchBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    public BranchBuilder withAddress(String address) {
        this.address = address;
        return this;
    }

    public BranchBuilder withBranchName(String branchName) {
        this.branchName = branchName;
        return this;
    }

    public BranchBuilder withFacilityEmployees(List<Employee> facilityEmployees) {
        this.facilityEmployees = facilityEmployees;
        return this;
    }

    public BranchBuilder withAvailableCars(List<Car> availableCars) {
        this.availableCars = availableCars;
        return this;
    }

    public BranchBuilder withRental(Rental rental) {
        this.rental = rental;
        return this;
    }

    public Branch build() {
        Branch branch = new Branch();
        branch.setId(id);
        branch.setAddress(address);
        branch.setBranchName(branchName);
        branch.setFacilityEmployees(facilityEmployees);
        branch.setAvailableCars(availableCars);
        branch.setRental(rental);
        return branch;
    }
}
