package ro.sda.javaremote26.finalproject.enums;

public enum Brand {
    BMW,
    AUDI,
    TOYOTA,
    NISSAN,
    RENAULT,
    LAMBORGHINI,
    MERCEDES;

}
