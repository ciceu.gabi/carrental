package ro.sda.javaremote26.finalproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sda.javaremote26.finalproject.dto.RefundDto;
import ro.sda.javaremote26.finalproject.entities.Refund;
import ro.sda.javaremote26.finalproject.mapper.RefundMapper;
import ro.sda.javaremote26.finalproject.repository.RefundRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RefundService {

    private RefundRepository refundRepository;
    private RefundMapper refundMapper;

    @Autowired
    public RefundService(RefundRepository refundRepository, RefundMapper refundMapper) {
        this.refundRepository = refundRepository;
        this.refundMapper = refundMapper;
    }

    public List<RefundDto> findAll(){
        List<Refund> refundList = refundRepository.findAll();
        List<RefundDto> refundDto = refundList.stream().map(refund -> refundMapper.mapToDto(refund)).collect(Collectors.toList());
        return refundDto;
    }
}
