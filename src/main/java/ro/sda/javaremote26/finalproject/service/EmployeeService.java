package ro.sda.javaremote26.finalproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sda.javaremote26.finalproject.dto.EmployeeDto;
import ro.sda.javaremote26.finalproject.entities.Employee;
import ro.sda.javaremote26.finalproject.mapper.EmployeeMapper;
import ro.sda.javaremote26.finalproject.repository.EmployeeRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService {

    private EmployeeRepository employeeRepository;

    private EmployeeMapper employeeMapper;

    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository, EmployeeMapper employeeMapper) {
        this.employeeRepository = employeeRepository;
        this.employeeMapper = employeeMapper;
    }

    public List<EmployeeDto> findAll() {
        List<Employee> employeeList = employeeRepository.findAll();
        List<EmployeeDto> employeeDto = employeeList.stream().map(employee -> employeeMapper.mapToDto(employee)).collect(Collectors.toList());
        return employeeDto;
    }

}
