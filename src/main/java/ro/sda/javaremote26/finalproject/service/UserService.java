package ro.sda.javaremote26.finalproject.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ro.sda.javaremote26.finalproject.dto.UserDto;
import ro.sda.javaremote26.finalproject.dto.UserHeaderDto;
import ro.sda.javaremote26.finalproject.entities.Role;
import ro.sda.javaremote26.finalproject.entities.User;
import ro.sda.javaremote26.finalproject.mapper.UserMapper;
import ro.sda.javaremote26.finalproject.repository.RoleRepository;
import ro.sda.javaremote26.finalproject.repository.UserRepository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private final UserMapper userMapper;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;


    @Autowired
    public UserService(UserMapper userMapper, UserRepository userRepository, RoleRepository roleRepository,
                       BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userMapper = userMapper;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public void register(UserDto userDto) {
        User user = userMapper.map(userDto);
        assignRolesTo(user, userDto);
        encodePasswordFor(user);
        userRepository.save(user);
    }

    private void encodePasswordFor(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
    }

    private void assignRolesTo(User user, UserDto userDto) {
        Optional<Role> optionalRole;

        if (userDto.isAdmin()) {
            optionalRole = roleRepository.findByName("ROLE_ADMIN");
        } else {
            optionalRole = roleRepository.findByName("ROLE_USER");
        }

        if (optionalRole.isPresent()) {
            Role role = optionalRole.get();
            user.setRole(role);
        }
    }

    public UserHeaderDto getUserHeaderDto(String loggedInUserEmail) {
        Optional<User> optionalUser = userRepository.findByEmail(loggedInUserEmail);
        if (optionalUser.isPresent()) {
            return userMapper.mapUserToUserHeader(optionalUser.get());
        }
        throw new RuntimeException("Invalid user email !");
    }

    public List<User> getUsersForReservations(){
        UserDetails user = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (((SimpleGrantedAuthority) (user.getAuthorities().toArray()[0])).getAuthority().equals("ROLE_ADMIN")){
            return userRepository.findAll();
        }else{
            return Arrays.asList(userRepository.findByEmail(user.getUsername()).get());
        }
    }
    public void updateUser(UserDto userDto){
        User user = userMapper.map(userDto);
        encodePasswordFor(user);
        userRepository.save(user);
    }
}