package ro.sda.javaremote26.finalproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sda.javaremote26.finalproject.dto.LoanDto;
import ro.sda.javaremote26.finalproject.entities.Loan;
import ro.sda.javaremote26.finalproject.mapper.LoanMapper;
import ro.sda.javaremote26.finalproject.repository.LoanRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class LoanService {
    private LoanRepository loanRepository;
    private LoanMapper loanMapper;

    @Autowired
    public LoanService(LoanRepository loanRepository, LoanMapper loanMapper) {
        this.loanRepository = loanRepository;
        this.loanMapper = loanMapper;
    }

    public List<LoanDto> findAll() {
        List<Loan> loanList = loanRepository.findAll();
        List<LoanDto> loanDto = loanList.stream().map(loan -> loanMapper.mapToDto(loan)).collect(Collectors.toList());
        return loanDto;
    }
}
