package ro.sda.javaremote26.finalproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sda.javaremote26.finalproject.dto.BranchDto;
import ro.sda.javaremote26.finalproject.dto.ReservationDto;
import ro.sda.javaremote26.finalproject.mapper.BranchMapper;
import ro.sda.javaremote26.finalproject.repository.BranchRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BranchService {

    private BranchRepository branchRepository;
    private BranchMapper branchMapper;

    @Autowired
    public BranchService(BranchRepository branchRepository, BranchMapper branchMapper) {
        this.branchRepository = branchRepository;
        this.branchMapper = branchMapper;
    }

    public List<BranchDto> findAll() {
        return branchRepository.findAll().stream().map(branch -> branchMapper.mapToDto(branch)).collect(Collectors.toList());
    }
}
