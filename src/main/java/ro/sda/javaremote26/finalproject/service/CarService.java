package ro.sda.javaremote26.finalproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sda.javaremote26.finalproject.dto.CarDto;
import ro.sda.javaremote26.finalproject.entities.Car;
import ro.sda.javaremote26.finalproject.enums.Brand;
import ro.sda.javaremote26.finalproject.mapper.CarMapper;
import ro.sda.javaremote26.finalproject.repository.CarRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CarService {
    private CarRepository carRepository;
    private CarMapper carMapper;

    @Autowired
    public CarService(CarRepository carRepository, CarMapper carMapper) {
        this.carRepository = carRepository;
        this.carMapper = carMapper;
    }

    public List<CarDto> findAll() {
        List<Car> carList = carRepository.findAll();
        List<CarDto> carDto = carList.stream().map(car -> carMapper.mapToDto(car)).collect(Collectors.toList());
        return carDto;
    }


    public CarDto findById(int carId) {
        Optional<Car> carEntity = carRepository.findById(carId);
        if (carEntity.isEmpty()) {
            throw new RuntimeException("Car not found");
        }
        return carMapper.mapToDto(carEntity.get());
    }

    public void save(CarDto carDto){
        Car car = carMapper.mapToEntity(carDto);
        carRepository.save(car);
    }
}
