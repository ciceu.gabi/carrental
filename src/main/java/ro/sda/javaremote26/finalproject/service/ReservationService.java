package ro.sda.javaremote26.finalproject.service;

import org.apache.catalina.LifecycleState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sda.javaremote26.finalproject.dto.ReservationDto;
import ro.sda.javaremote26.finalproject.entities.Reservation;
import ro.sda.javaremote26.finalproject.mapper.ReservationMapper;
import ro.sda.javaremote26.finalproject.repository.ReservationRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ReservationService {

    private ReservationRepository reservationRepository;
    private ReservationMapper reservationMapper;

    @Autowired
    public ReservationService(ReservationRepository reservationRepository, ReservationMapper reservationMapper) {
        this.reservationRepository = reservationRepository;
        this.reservationMapper = reservationMapper;
    }

    public List<ReservationDto> findAll() {
        List<Reservation> reservationList = reservationRepository.findAll();
        List<ReservationDto> reservationDto = reservationList.stream().map(reservation -> reservationMapper.mapToDto(reservation)).collect(Collectors.toList());
        return reservationDto;
    }

    public void save(ReservationDto reservationDto){
        Reservation reservationEntity = reservationMapper.mapToEntity(reservationDto);
        reservationRepository.save(reservationEntity);
    }

    public void deleteById(int id) {
        reservationRepository.deleteById(id);
    }

    public ReservationDto findById(int id) {
        Optional<Reservation> result = reservationRepository.findById(id);
        if (result.isEmpty()) {
            throw new RuntimeException("Reservation not found");
        }

       return reservationMapper.mapToDto(result.get());
    }
}
