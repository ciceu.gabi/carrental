package ro.sda.javaremote26.finalproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.sda.javaremote26.finalproject.entities.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
}
