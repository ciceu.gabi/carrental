package ro.sda.javaremote26.finalproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.sda.javaremote26.finalproject.entities.Refund;

public interface RefundRepository extends JpaRepository<Refund, Integer> {
}
