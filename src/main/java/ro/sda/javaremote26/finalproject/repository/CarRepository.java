package ro.sda.javaremote26.finalproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.sda.javaremote26.finalproject.entities.Car;
import ro.sda.javaremote26.finalproject.enums.Brand;

import java.util.List;

public interface CarRepository extends JpaRepository<Car, Integer> {

    List<Car> findAllByBrandAndBodyTypeAndModelAndYear(Brand brand, String bodyType, String model, int year);
}
