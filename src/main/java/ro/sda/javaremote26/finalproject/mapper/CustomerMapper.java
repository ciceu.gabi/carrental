package ro.sda.javaremote26.finalproject.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sda.javaremote26.finalproject.dto.CustomerDto;
import ro.sda.javaremote26.finalproject.entities.Customer;
import ro.sda.javaremote26.finalproject.repository.CustomerRepository;

@Service
public class CustomerMapper implements Mapper<Customer, CustomerDto>{
    @Autowired
    CustomerRepository customerRepository;

    @Override
    public CustomerDto mapToDto(Customer customer) {
        CustomerDto customerDto = new CustomerDto();
        customerDto.setId(customer.getId());
        customerDto.setAddress(customer.getAddress());
        customerDto.setEmail(customer.getEmail());
        customerDto.setFirstName(customer.getFirstName());
        customerDto.setName(customer.getName());
        return customerDto;
    }

    @Override
    public Customer mapToEntity(CustomerDto customerDto) {
        Customer customer = new Customer();
        if (customerDto.getId() != null){
            customer = customerRepository.getById(customerDto.getId());
        }
        customer.setAddress(customerDto.getAddress());
        customer.setEmail(customerDto.getEmail());
        customer.setFirstName(customerDto.getFirstName());
        customer.setName(customerDto.getName());
        return  customer;
    }
}
