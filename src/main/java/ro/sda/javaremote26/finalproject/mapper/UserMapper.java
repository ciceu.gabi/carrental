package ro.sda.javaremote26.finalproject.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.sda.javaremote26.finalproject.dto.UserDto;
import ro.sda.javaremote26.finalproject.dto.UserHeaderDto;
import ro.sda.javaremote26.finalproject.entities.Role;
import ro.sda.javaremote26.finalproject.entities.User;
import ro.sda.javaremote26.finalproject.repository.RoleRepository;
import ro.sda.javaremote26.finalproject.repository.UserRepository;

import javax.validation.constraints.NotNull;
import java.util.List;

@Component
public class UserMapper {
    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    public User map(UserDto userDto) {
        User user = new User();
        if (userDto.getId() != null) {
            user = userRepository.findById(userDto.getId()).orElse(new User());
        }
        user.setName(userDto.getName());
        user.setEmail(userDto.getEmail());
        user.setPassword(userDto.getPassword());
        user.setFirstName(userDto.getFirstName());
        user.setAddress(userDto.getAddress());
        user.setCountry(userDto.getCountry());
        if (userDto.getRoleId() != null) {
            Role role = roleRepository.findById(userDto.getRoleId()).get();
            user.setRole(role);
        }

        return user;
    }


    public UserHeaderDto mapUserToUserHeader(User user) {
        UserHeaderDto userHeaderDto = new UserHeaderDto();
        userHeaderDto.setName(user.getName());
        return userHeaderDto;
    }

    public UserDto map(@NotNull User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setName(user.getName());
        userDto.setEmail(user.getEmail());
        userDto.setPassword(user.getPassword());
        userDto.setFirstName(user.getFirstName());
        userDto.setAddress(user.getAddress());
        userDto.setCountry(user.getCountry());
        userDto.setRoleId(user.getRole().getId());
        List<Role> availableRoles= roleRepository.findAll();
        userDto.setAvailableRoles(availableRoles);
        return userDto;
    }


}
