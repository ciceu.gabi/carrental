package ro.sda.javaremote26.finalproject.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sda.javaremote26.finalproject.dto.ReservationDto;
import ro.sda.javaremote26.finalproject.entities.Reservation;
import ro.sda.javaremote26.finalproject.repository.*;

@Service
public class ReservationMapper implements Mapper<Reservation, ReservationDto>{

    @Autowired
    ReservationRepository reservationRepository;
    @Autowired
    BranchRepository branchRepository;
    @Autowired
    CarRepository carRepository;
    @Autowired
    UserRepository userRepository;

    @Override
    public ReservationDto mapToDto(Reservation reservation) {
        ReservationDto reservationDto = new ReservationDto();
        reservationDto.setId(reservation.getId());
        reservationDto.setAmount(reservation.getAmount());
        reservationDto.setDateFrom(reservation.getDateFrom());
        reservationDto.setDateTo(reservation.getDateTo());
        reservationDto.setDateOfBooking(reservation.getDateOfBooking());
        if (reservation.getBranchOfLoan() != null){
            reservationDto.setBranchId(reservation.getBranchOfLoan().getId());
            reservationDto.setBranchName(reservation.getBranchOfLoan().getBranchName());
        }
        if (reservation.getCar() != null){
            reservationDto.setCarId(reservation.getCar().getId());
            reservationDto.setCarName(reservation.getCar().getBrand().name());
        }
        if (reservation.getClient() != null){
            reservationDto.setCustomerId(reservation.getClient().getId());
            reservationDto.setClient(reservation.getClient().getFirstName() + " " + reservation.getClient().getName());
        }
        if (reservation.getReturnDepartment() != null){
            reservationDto.setReturnDepartmentId(reservation.getReturnDepartment().getId());
            reservationDto.setReturnDepartmentName(reservation.getReturnDepartment().getBranchName());
        }
        return reservationDto;
    }

    @Override
    public Reservation mapToEntity(ReservationDto reservationDto) {
        Reservation reservation = new Reservation();
        if (reservationDto.getId() != null){
            reservation = reservationRepository.getById(reservationDto.getId());
        }
        reservation.setAmount(reservationDto.getAmount());
        reservation.setDateFrom(reservationDto.getDateFrom());
        reservation.setDateTo(reservationDto.getDateTo());
        reservation.setDateOfBooking(reservationDto.getDateOfBooking());
        if (reservationDto.getBranchId() != null){
            reservation.setBranchOfLoan(branchRepository.getById(reservationDto.getBranchId()));
        }
        if (reservationDto.getCarId() != null) {
            reservation.setCar(carRepository.getById(reservationDto.getCarId()));
        }
        if (reservationDto.getCustomerId() != null) {
            reservation.setClient(userRepository.getById(reservationDto.getCustomerId()));
        }
        if (reservationDto.getReturnDepartmentId() != null) {
            reservation.setReturnDepartment(branchRepository.getById(reservationDto.getReturnDepartmentId()));
        }
        return reservation;
    }
}
