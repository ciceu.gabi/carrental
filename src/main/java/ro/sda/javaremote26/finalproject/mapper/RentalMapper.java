package ro.sda.javaremote26.finalproject.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sda.javaremote26.finalproject.dto.RentalDto;
import ro.sda.javaremote26.finalproject.entities.Rental;
import ro.sda.javaremote26.finalproject.repository.CustomerRepository;
import ro.sda.javaremote26.finalproject.repository.RentalRepository;

@Service
public class RentalMapper implements Mapper<Rental, RentalDto>{

    @Autowired
    RentalRepository rentalRepository;
    @Autowired
    CustomerRepository customerRepository;

    @Override
    public RentalDto mapToDto(Rental rental) {
        RentalDto rentalDto = new RentalDto();
        rentalDto.setId(rental.getId());
        rentalDto.setInternetDomain(rental.getInternetDomain());
        rentalDto.setLogoType(rental.getLogoType());
        rentalDto.setName(rental.getName());
        rentalDto.setContactAddress(rental.getContactAddress());
        if (rental.getOwner() != null){
            rentalDto.setOwnerId(rental.getOwner().getId());
        }
        return rentalDto;
    }

    @Override
    public Rental mapToEntity(RentalDto rentalDto) {
        Rental rental = new Rental();
        if (rentalDto.getId() != null){
            rental = rentalRepository.getById(rentalDto.getId());
        }
        rental.setInternetDomain(rentalDto.getInternetDomain());
        rental.setName(rentalDto.getName());
        rental.setLogoType(rentalDto.getLogoType());
        rental.setContactAddress(rentalDto.getContactAddress());
        if (rentalDto.getOwnerId() != null){
            rental.setOwner(customerRepository.getById(rentalDto.getOwnerId()));
        }
        return rental;
    }
}
