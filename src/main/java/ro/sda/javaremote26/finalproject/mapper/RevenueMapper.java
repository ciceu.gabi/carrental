package ro.sda.javaremote26.finalproject.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sda.javaremote26.finalproject.dto.RevenueDto;
import ro.sda.javaremote26.finalproject.entities.Revenue;
import ro.sda.javaremote26.finalproject.repository.RevenueRepository;

@Service
public class RevenueMapper implements Mapper<Revenue, RevenueDto> {
    @Autowired
    RevenueRepository revenueRepository;

    @Override
    public RevenueDto mapToDto(Revenue revenue) {
        RevenueDto revenueDto = new RevenueDto();
        revenueDto.setId(revenue.getId());
        revenueDto.setAmount(revenue.getAmount());
        return revenueDto;
    }

    @Override
    public Revenue mapToEntity(RevenueDto revenueDto) {
        Revenue revenue = new Revenue();
        if (revenueDto.getId() != null) {
            revenue = revenueRepository.getById(revenueDto.getId());
        }
        revenue.setAmount(revenueDto.getAmount());
        return revenue;
    }
}
