package ro.sda.javaremote26.finalproject.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sda.javaremote26.finalproject.dto.EmployeeDto;
import ro.sda.javaremote26.finalproject.entities.Employee;
import ro.sda.javaremote26.finalproject.repository.BranchRepository;
import ro.sda.javaremote26.finalproject.repository.EmployeeRepository;

@Service
public class EmployeeMapper implements Mapper<Employee, EmployeeDto>{

    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    BranchRepository branchRepository;

    @Override
    public EmployeeDto mapToDto(Employee employee) {
        EmployeeDto employeeDto = new EmployeeDto();
        employeeDto.setId(employee.getId());
        employeeDto.setFirstName(employee.getFirstName());
        employeeDto.setName(employee.getName());
        employeeDto.setPosition(employee.getPosition());
        if (employee.getBranch() != null){
            employeeDto.setBranch(employee.getBranch().getId());
        }
        if (employee.getImage() != null){
            employeeDto.setImage(employee.getImage());
        }
        return employeeDto;
    }

    @Override
    public Employee mapToEntity(EmployeeDto employeeDto) {
        Employee employee = new Employee();
        if (employeeDto.getId() != null){
            employee = employeeRepository.getById(employeeDto.getId());
        }
        employee.setName(employeeDto.getName());
        employee.setFirstName(employeeDto.getFirstName());
        employee.setPosition(employeeDto.getPosition());
        if (employeeDto.getBranch() != null){
            employee.setBranch(branchRepository.getById(employeeDto.getBranch()));
        }
        return employee;
    }
}
