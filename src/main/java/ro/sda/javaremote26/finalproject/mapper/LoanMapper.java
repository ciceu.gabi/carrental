package ro.sda.javaremote26.finalproject.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sda.javaremote26.finalproject.dto.LoanDto;
import ro.sda.javaremote26.finalproject.entities.Loan;
import ro.sda.javaremote26.finalproject.repository.EmployeeRepository;
import ro.sda.javaremote26.finalproject.repository.LoanRepository;
import ro.sda.javaremote26.finalproject.repository.ReservationRepository;

@Service
public class LoanMapper implements Mapper<Loan, LoanDto>{

    @Autowired
    LoanRepository loanRepository;
    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    ReservationRepository reservationRepository;

    @Override
    public LoanDto mapToDto(Loan loan) {
        LoanDto loanDto = new LoanDto();
        loanDto.setId(loan.getId());
        loanDto.setComments(loan.getComments());
        loanDto.setDateOfRental(loan.getDateOfRental());
        if (loan.getEmployee() != null){
            loanDto.setEmployee(loan.getEmployee().getId());
        }
        if (loan.getReservation() != null){
            loanDto.setReservation(loan.getReservation().getId());
        }
        return loanDto;
    }

    @Override
    public Loan mapToEntity(LoanDto loanDto) {
        Loan loan = new Loan();
        if (loanDto.getId() != null){
            loan = loanRepository.getById(loanDto.getId());
        }
        loan.setComments(loanDto.getComments());
        loan.setDateOfRental(loanDto.getDateOfRental());
        if (loanDto.getEmployee() != null){
            loan.setEmployee(employeeRepository.getById(loanDto.getEmployee()));
        }
        if (loanDto.getReservation() != null){
            loan.setReservation(reservationRepository.getById(loanDto.getReservation()));
        }
        return loan;
    }
}
