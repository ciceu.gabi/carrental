package ro.sda.javaremote26.finalproject.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sda.javaremote26.finalproject.dto.RefundDto;
import ro.sda.javaremote26.finalproject.entities.Refund;
import ro.sda.javaremote26.finalproject.repository.EmployeeRepository;
import ro.sda.javaremote26.finalproject.repository.RefundRepository;
import ro.sda.javaremote26.finalproject.repository.ReservationRepository;

@Service
public class RefundMapper implements  Mapper<Refund, RefundDto>{
    @Autowired
    RefundRepository refundRepository;
    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    ReservationRepository reservationRepository;

    @Override
    public RefundDto mapToDto(Refund refund) {
        RefundDto refundDto = new RefundDto();
        refundDto.setId(refund.getId());
        refundDto.setComments(refund.getComments());
        refundDto.setDateOfReturn(refund.getDateOfReturn());
        refundDto.setSurcharge(refund.getSurcharge());
        if (refund.getEmployee() != null) {
            refundDto.setEmployee(refund.getEmployee().getId());
        }
        if (refund.getReservation() != null) {
            refundDto.setReservation(refund.getReservation().getId());
        }
        return refundDto;
    }

    @Override
    public Refund mapToEntity(RefundDto refundDto) {
        Refund refund = new Refund();
        if (refundDto.getId() != null){
            refund = refundRepository.getById(refundDto.getId());
        }
        refund.setComments(refundDto.getComments());
        refund.setDateOfReturn(refundDto.getDateOfReturn());
        refund.setSurcharge(refundDto.getSurcharge());
        if (refundDto.getEmployee() != null){
            refund.setEmployee(employeeRepository.getById(refundDto.getEmployee()));
        }
        if (refundDto.getReservation() != null){
            refund.setReservation(reservationRepository.getById(refundDto.getReservation()));
        }
        return refund;
    }
}
