package ro.sda.javaremote26.finalproject.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sda.javaremote26.finalproject.dto.CarDto;
import ro.sda.javaremote26.finalproject.entities.Car;
import ro.sda.javaremote26.finalproject.repository.BranchRepository;
import ro.sda.javaremote26.finalproject.repository.CarRepository;

@Service
public class CarMapper implements Mapper<Car, CarDto> {

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private BranchMapper branchMapper;

    @Autowired
    private BranchRepository branchRepository;

    public CarDto mapToDto(Car car) {
        CarDto carDto = new CarDto();
        carDto.setId(car.getId());
        carDto.setModel(car.getModel());
        carDto.setBodyType(car.getBodyType());
        carDto.setYear(car.getYear());
        carDto.setColor(car.getColor());
        carDto.setKilometers(car.getKilometers());
        carDto.setStatus(car.getStatus());
        carDto.setAmountPerDay(car.getAmountPerDay());
        carDto.setBrand(car.getBrand());
        carDto.setDescription(car.getDescription());
        if (car.getBranch() != null) {
            carDto.setBranch(branchMapper.mapToDto(car.getBranch()));
        }
        if (car.getImage() != null) {
            carDto.setImage(car.getImage());
        }
        return carDto;
    }

    public Car mapToEntity(CarDto carDto) {
        Car car = new Car();
        if (carDto.getId() != null) {
            car = carRepository.getById(carDto.getId());
        }
        car.setBrand(carDto.getBrand());
        car.setModel(carDto.getModel());
        car.setBodyType(carDto.getBodyType());
        car.setYear(carDto.getYear());
        car.setColor(carDto.getColor());
        car.setKilometers(carDto.getKilometers());
        car.setStatus(carDto.getStatus());
        car.setAmountPerDay(carDto.getAmountPerDay());
        car.setDescription(carDto.getDescription());
        if (carDto.getBranch() != null) {
            car.setBranch(branchRepository.findById(carDto.getBranchId()).orElse(null));
        }
        return car;
    }
}
