package ro.sda.javaremote26.finalproject.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sda.javaremote26.finalproject.dto.BranchDto;
import ro.sda.javaremote26.finalproject.entities.Branch;
import ro.sda.javaremote26.finalproject.repository.BranchRepository;
import ro.sda.javaremote26.finalproject.repository.RentalRepository;

@Service
public class BranchMapper implements Mapper<Branch, BranchDto>{

    @Autowired
    BranchRepository branchRepository;
    @Autowired
    RentalRepository rentalRepository;


    @Override
    public BranchDto mapToDto(Branch branch) {
        BranchDto branchDto = new BranchDto();
        branchDto.setId(branch.getId());
        branchDto.setAddress(branch.getAddress());
        branchDto.setBranchName(branch.getBranchName());
        if (branch.getRental() != null) {
            branchDto.setRentalId(branch.getRental().getId());
        }
        return branchDto;
    }

    @Override
    public Branch mapToEntity(BranchDto branchDto) {
        Branch branch = new Branch();
        if (branchDto.getId() != null){
            branch = branchRepository.getById(branchDto.getId());
        }
        branch.setAddress(branchDto.getAddress());
        branch.setBranchName(branchDto.getBranchName());
        if (branchDto.getRentalId() != null){
            branch.setRental(rentalRepository.getById(branchDto.getRentalId()));
        }
        return branch;
    }
}
