package ro.sda.javaremote26.finalproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ro.sda.javaremote26.finalproject.dto.CarDto;
import ro.sda.javaremote26.finalproject.entities.Car;
import ro.sda.javaremote26.finalproject.enums.Brand;
import ro.sda.javaremote26.finalproject.enums.Status;
import ro.sda.javaremote26.finalproject.mapper.CarMapper;
import ro.sda.javaremote26.finalproject.repository.BranchRepository;
import ro.sda.javaremote26.finalproject.repository.CarRepository;
import ro.sda.javaremote26.finalproject.service.BranchService;
import ro.sda.javaremote26.finalproject.service.CarService;
import ro.sda.javaremote26.finalproject.utils.ImageUtil;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/car")
public class CarController {
    @Autowired
    CarRepository carRepository;
    @Autowired
    BranchRepository branchRepository;
    @Autowired
    CarMapper carMapper;
    @Autowired
    CarService carService;
    @Autowired
    BranchService branchService;

    @GetMapping("/")
    public String showAllCars(Model model) {
        model.addAttribute("carList", carService.findAll());
        model.addAttribute("imgUtil", new ImageUtil());
        return "carList";
    }

    @GetMapping("/add")
    public String showAddForm(Model model) {
        CarDto carDto = new CarDto();
        model.addAttribute("carDto", carDto);
        model.addAttribute("availableBranches", branchService.findAll());
        model.addAttribute("statusValues", Status.values());
        model.addAttribute("brandValues", Brand.values());
        return "addCar";
    }

    @PostMapping("/add")
    public String addCar(@RequestParam("file") MultipartFile file, CarDto carDto, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "redirect:/add";
        }
        Car car = carMapper.mapToEntity(carDto);
        if (!file.isEmpty()) {
            car.setImage(ImageUtil.resizeAndCrop(file, 300, 175));
        }
        carRepository.save(car);
        return "redirect:/car/";
    }

    @GetMapping("/edit/{id}")
    public String showEditForm(@PathVariable("id") int id, Model model) {
        Optional<Car> result = carRepository.findById(id);
        if (result.isEmpty()) {
            throw new RuntimeException("Car not found");
        } else {
            CarDto carDto = carMapper.mapToDto(result.get());
            model.addAttribute("carDto", carDto);
        }
        model.addAttribute("availableBranches", branchRepository.findAll());
        model.addAttribute("statusValues", Status.values());
        model.addAttribute("brandValues", Brand.values());
        return "editCar";
    }

    @PostMapping("/edit/{id}")
    public String editCar(@PathVariable("id") int id, @RequestParam("file") MultipartFile file, CarDto carDto, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            carDto.setId(id);
            return "editCar";
        }
        Car car = carMapper.mapToEntity(carDto);

        if (!file.isEmpty()) {
            car.setImage(ImageUtil.resizeAndCrop(file, 300, 175));
        }
        carRepository.save(car);
        return "redirect:/car/";
    }

    @GetMapping("/delete/{id}")
    public String deleteCar(@PathVariable("id") int id) {
        carRepository.deleteById(id);
        return "redirect:/car/";
    }

    @PostMapping("/searchCar")
    public String showSearchedCars(CarDto searchedCar, Model model) {
        List<Car> result = carRepository.findAllByBrandAndBodyTypeAndModelAndYear
                (searchedCar.getBrand(), searchedCar.getBodyType(), searchedCar.getModel(), searchedCar.getYear());
        ControlerUtil.initMainPage(model, result);
        return "listing";
    }
}
