package ro.sda.javaremote26.finalproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ro.sda.javaremote26.finalproject.dto.CarDto;
import ro.sda.javaremote26.finalproject.dto.RefundDto;
import ro.sda.javaremote26.finalproject.entities.Car;
import ro.sda.javaremote26.finalproject.entities.Refund;
import ro.sda.javaremote26.finalproject.enums.Brand;
import ro.sda.javaremote26.finalproject.enums.Status;
import ro.sda.javaremote26.finalproject.mapper.RefundMapper;
import ro.sda.javaremote26.finalproject.repository.EmployeeRepository;
import ro.sda.javaremote26.finalproject.repository.RefundRepository;
import ro.sda.javaremote26.finalproject.repository.ReservationRepository;
import ro.sda.javaremote26.finalproject.service.EmployeeService;
import ro.sda.javaremote26.finalproject.service.RefundService;
import ro.sda.javaremote26.finalproject.service.ReservationService;

import java.util.Optional;

@Controller
@RequestMapping("/refund")
public class RefundController {
    @Autowired
    RefundRepository refundRepository;
    @Autowired
    ReservationRepository reservationRepository;
    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    RefundMapper refundMapper;
    @Autowired
    EmployeeService employeeService;
    @Autowired
    ReservationService reservationService;
    @Autowired
    RefundService refundService;

    @GetMapping("/")
    public String showAllRefunds(Model model){
        model.addAttribute("refundList",refundService.findAll());
        return "refundList";
    }
    @GetMapping("/add")
    public String showAddForm(Model model){
        RefundDto refundDto = new RefundDto();
        model.addAttribute("refundDto",refundDto);
        model.addAttribute("availableEmployees", employeeService.findAll());
        model.addAttribute("availableReservations", reservationService.findAll());
        return "addRefund";
    }

    @PostMapping("/add")
    public String addRefund(RefundDto refundDto, BindingResult bindingResult, Model model){
        if (bindingResult.hasErrors()){
            return "addRefund";
        }
        Refund refund = refundMapper.mapToEntity(refundDto);
        refundRepository.save(refund);
        return "redirect:/refund/";
    }

    @GetMapping("/edit/{id}")
    public String showEditForm(@PathVariable("id") int id, Model model){
        Optional<Refund> result = refundRepository.findById(id);
        if (result.isEmpty()){
            throw new RuntimeException("Refund not found");
        }else {
           RefundDto refundDto = refundMapper.mapToDto(result.get());
            model.addAttribute("refundDto", refundDto);
        }
        model.addAttribute("availableEmployees", employeeService.findAll());
        model.addAttribute("availableReservations", reservationService.findAll());
        return "editRefund";
    }
    @PostMapping("/edit/{id}")
    public String editRefund(@PathVariable("id") int id, RefundDto refundDto, BindingResult bindingResult, Model model){
        if (bindingResult.hasErrors()){
            refundDto.setId(id);
            return "editRefund";
        }
        Refund refund = refundMapper.mapToEntity(refundDto);
        refundRepository.save(refund);
        return "redirect:/refund/";
    }
    @GetMapping("/delete/{id}")
    public String deleteRefund(@PathVariable("id") int id){
        refundRepository.deleteById(id);
        return "redirect:/refund/";
    }
}
