package ro.sda.javaremote26.finalproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import ro.sda.javaremote26.finalproject.dto.ContactDto;
import ro.sda.javaremote26.finalproject.repository.*;
import ro.sda.javaremote26.finalproject.service.EmployeeService;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.validation.Valid;

@Controller
public class MainController {

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private BranchRepository branchRepository;

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/")
    public String index(Model model) {
        ControlerUtil.initMainPage(model, carRepository.findAll());
        return "index";
    }

    @GetMapping("/about")
    public String about(Model model) {
        model.addAttribute("employees", employeeRepository.findAll());
        model.addAttribute("totalCars", carRepository.count());
        model.addAttribute("totalBranches", branchRepository.count());
        model.addAttribute("totalCustomers", userRepository.count());
        return "about";
    }

    @GetMapping("/blog")
    public String blog(Model model) {
        return "blog";
    }

    @GetMapping("/testimonials")
    public String testimonials(Model model) {
        return "testimonials";
    }

    @GetMapping("/listing")
    public String listing(Model model) {
        ControlerUtil.initMainPage(model, carRepository.findAll());
        return "listing";
    }

    @GetMapping("/contact")
    public String contact(Model model) {
        ContactDto contact = new ContactDto();
        model.addAttribute("contact", contact);
        return "contact";
    }

    @PostMapping("/contact")
    public String sendEmail(@Valid ContactDto contact, BindingResult bindingResult, Model model) {
        MimeMessagePreparator preparator = new MimeMessagePreparator() {
            public void prepare(MimeMessage mimeMessage) throws Exception {
                mimeMessage.setRecipient(Message.RecipientType.CC,
                        new InternetAddress("ciceu.gabi@gmail.com"));
                mimeMessage.setRecipient(Message.RecipientType.TO,
                        new InternetAddress("sda.test.spring@gmail.com"));
                mimeMessage.setFrom(new InternetAddress(contact.getEmail()));
                mimeMessage.setText(contact.getMessage());
                mimeMessage.setSubject("New contact from: " + contact.getFirstName() + " " + contact.getName());
            }
        };
        try {
            this.mailSender.send(preparator);
        } catch (MailException ex) {
            // simply log it and go on...
            System.err.println(ex.getMessage());
        }

        return "redirect:/contact";
    }
}
