package ro.sda.javaremote26.finalproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ro.sda.javaremote26.finalproject.dto.ReservationDto;
import ro.sda.javaremote26.finalproject.enums.Status;
import ro.sda.javaremote26.finalproject.service.BranchService;
import ro.sda.javaremote26.finalproject.service.CarService;
import ro.sda.javaremote26.finalproject.service.ReservationService;
import ro.sda.javaremote26.finalproject.service.UserService;

@Controller
@RequestMapping("/reservation")
public class ReservationController {

    @Autowired
    ReservationService reservationService;
    @Autowired
    UserService userService;
    @Autowired
    CarService carService;
    @Autowired
    BranchService branchService;

    @GetMapping("/")
    public String showAllReservations(Model model) {
        model.addAttribute("reservationList", reservationService.findAll());
        return "reservationList";
    }

    @GetMapping("/add")
    public String showAddForm(Model model) {
        ReservationDto reservationDto = new ReservationDto();
        model.addAttribute("reservationDto", reservationDto);
        model.addAttribute("availableBranches", branchService.findAll());
        model.addAttribute("availableDepartments", branchService.findAll());
        model.addAttribute("availableCars", carService.findAll());
        model.addAttribute("availableCustomers", userService.getUsersForReservations());

        model.addAttribute("statusValues", Status.values());
        return "addReservation";
    }

    @PostMapping("/add")
    public String addReservation(ReservationDto reservationDto, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "addReservation";
        }
        reservationService.save(reservationDto);
        return "redirect:/";
    }

    @GetMapping("/edit/{id}")
    public String showEditForm(@PathVariable("id") int id, Model model) {

        model.addAttribute("reservationDto", reservationService.findById(id));
        model.addAttribute("availableBranches", branchService.findAll());
        model.addAttribute("availableDepartments", branchService.findAll());
        model.addAttribute("availableCars", carService.findAll());
        model.addAttribute("availableCustomers", userService.getUsersForReservations());
        model.addAttribute("statusValues", Status.values());
        return "editReservation";
    }

    @PostMapping("/edit/{id}")
    public String editReservation(@PathVariable("id") int id, ReservationDto reservationDto, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            reservationDto.setId(id);
            return "editReservation";
        }
        reservationService.save(reservationDto);
        return "redirect:/reservation/";
    }

    @GetMapping("/delete/{id}")
    public String deleteReservation(@PathVariable("id") int id) {
        reservationService.deleteById(id);
        return "redirect:/reservation/";
    }

    @GetMapping("/book/{id}")
    public String bookCar(@PathVariable("id") int car, Model model) {
        ReservationDto reservationDto = new ReservationDto();
        model.addAttribute("reservationDto", reservationDto);
        model.addAttribute("availableBranches", branchService.findAll());
        model.addAttribute("availableDepartments", branchService.findAll());
        model.addAttribute("availableCars", carService.findAll());
        model.addAttribute("availableCustomers", userService.getUsersForReservations());
        model.addAttribute("statusValues", Status.values());
        model.addAttribute("selectedCar", carService.findById(car));
        return "addReservation";
    }
}
