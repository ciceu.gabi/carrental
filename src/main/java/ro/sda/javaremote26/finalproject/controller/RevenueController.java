package ro.sda.javaremote26.finalproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ro.sda.javaremote26.finalproject.dto.CarDto;
import ro.sda.javaremote26.finalproject.dto.RevenueDto;
import ro.sda.javaremote26.finalproject.entities.Car;
import ro.sda.javaremote26.finalproject.entities.Revenue;
import ro.sda.javaremote26.finalproject.enums.Status;
import ro.sda.javaremote26.finalproject.mapper.RevenueMapper;
import ro.sda.javaremote26.finalproject.repository.RevenueRepository;

@Controller
@RequestMapping("/revenue")
public class RevenueController {
    @Autowired
    RevenueRepository revenueRepository;
    @Autowired
    RevenueMapper revenueMapper;

    @GetMapping("/")
    public String showAllRevenues(Model model){
        model.addAttribute("revenueList",revenueRepository.findAll());
        return "revenueList";
    }
    @GetMapping("/add")
    public String showAddForm(Model model){
        RevenueDto revenueDto = new RevenueDto();
        model.addAttribute("revenueDto",revenueDto);
        return "addRevenue";
    }

    @PostMapping("/add")
    public String addRevenue(RevenueDto revenueDto, BindingResult bindingResult, Model model){
        if (bindingResult.hasErrors()){
            return "addRevenue";
        }
       Revenue revenue = revenueMapper.mapToEntity(revenueDto);
        revenueRepository.save(revenue);
        return "redirect:/revenue/";
    }

    @GetMapping("/edit")
    public String showEditForm(Model model){
        return "editRevenue";
    }
    @PostMapping("/edit")
    public String editRevenue(Model model){
        return "addRevenue";
    }
}
