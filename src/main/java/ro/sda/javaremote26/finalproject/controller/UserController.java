package ro.sda.javaremote26.finalproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ro.sda.javaremote26.finalproject.dto.UserDto;
import ro.sda.javaremote26.finalproject.entities.User;
import ro.sda.javaremote26.finalproject.mapper.UserMapper;
import ro.sda.javaremote26.finalproject.repository.UserRepository;
import ro.sda.javaremote26.finalproject.service.UserService;

import javax.validation.Valid;
import java.util.List;

@Controller
public class UserController {
    @Autowired
    UserRepository userRepository;
    @Autowired
    UserService userService;
    @Autowired
    UserMapper userMapper;

    @GetMapping("/login")
    public String getLoginPage() {
        return "anotherLogin";
    }

    @GetMapping("/register")
    public String showSignUpForm(UserDto userDto) {
        return "user-register";
    }

    @PostMapping("/register")
    public String addUser(@Valid UserDto userDto, BindingResult result, Model model) {

        if (result.hasErrors()) {
            return "user-register";
        }
        userService.register(userDto);
        model.addAttribute("users", userRepository.findAll());
        return "redirect:/login";
    }

    @GetMapping("/users")
    public String showUpdateForm(Model model) {
        List<User> users = userRepository.findAll();
        model.addAttribute("users", users);
        return "users";
    }

    @GetMapping("users/edit/{id}")
    @Secured("ROLE_ADMIN")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        User user = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        UserDto userDto = userMapper.map(user);
        model.addAttribute("userDto", userDto);
        return "user-edit";
    }

    @PostMapping("users/update/{id}")
    @Secured("ROLE_ADMIN")
    public String updateUser(@PathVariable("id") long id, @Valid UserDto userDto, BindingResult result, Model model) {
        if (result.hasErrors()) {
            userDto.setId(id);
            return "user-edit";
        }

        userService.updateUser(userDto);
        model.addAttribute("users", userRepository.findAll());
        return "redirect:/customer/";
    }

    @GetMapping("users/delete/{id}")
    @Secured("ROLE_ADMIN")
    public String deleteUser(@PathVariable("id") long id, Model model) {
        User user = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        userRepository.delete(user);
        model.addAttribute("users", userRepository.findAll());
        return "redirect:/customer/";
    }
}
