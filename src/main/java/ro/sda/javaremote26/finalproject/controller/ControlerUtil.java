package ro.sda.javaremote26.finalproject.controller;

import org.springframework.ui.Model;
import ro.sda.javaremote26.finalproject.dto.CarDto;
import ro.sda.javaremote26.finalproject.entities.Car;
import ro.sda.javaremote26.finalproject.enums.Brand;

import java.util.List;

public class ControlerUtil {
    public static Model initMainPage(Model model, List<Car> result) {
        model.addAttribute("carList", result);
        model.addAttribute("searchedCar",new CarDto());
        model.addAttribute("availableBrands", Brand.values());
        return model;
    }
}
