package ro.sda.javaremote26.finalproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ro.sda.javaremote26.finalproject.dto.CarDto;
import ro.sda.javaremote26.finalproject.dto.RentalDto;
import ro.sda.javaremote26.finalproject.entities.Car;
import ro.sda.javaremote26.finalproject.entities.Rental;
import ro.sda.javaremote26.finalproject.enums.Brand;
import ro.sda.javaremote26.finalproject.enums.Status;
import ro.sda.javaremote26.finalproject.mapper.RentalMapper;
import ro.sda.javaremote26.finalproject.repository.CustomerRepository;
import ro.sda.javaremote26.finalproject.repository.RentalRepository;

import java.util.Optional;

@Controller
@RequestMapping("/rental")
public class RentalController {

    @Autowired
    RentalRepository rentalRepository;
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    RentalMapper rentalMapper;

    @GetMapping("/")
    public String showAllRentals(Model model){
        model.addAttribute("rentalList", rentalRepository.findAll());
        return "rentalList";
    }
    @GetMapping("/add")
    public String showAddForm(Model model){
        RentalDto rentalDto = new RentalDto();
        model.addAttribute("rentalDto",rentalDto);
        model.addAttribute("availableOwners", customerRepository.findAll());
        return "addRental";
    }

    @PostMapping("/add")
    public String addRental(RentalDto rentalDto, BindingResult bindingResult, Model model){
        if (bindingResult.hasErrors()){
            return "addRental";
        }
        Rental rental = rentalMapper.mapToEntity(rentalDto);
        rentalRepository.save(rental);
        return "redirect:/rental/";
    }

    @GetMapping("/edit/{id}")
    public String showEditForm(@PathVariable("id") int id, Model model){
        Optional<Rental> result = rentalRepository.findById(id);
        if (result.isEmpty()){
            throw new RuntimeException("Rental not found");
        }else {
           RentalDto rentalDto = rentalMapper.mapToDto(result.get());
            model.addAttribute("rentalDto", rentalDto);
        }
        model.addAttribute("availableOwners", customerRepository.findAll());
        return "editRental";
    }
    @PostMapping("/edit/{id}")
    public String editRental(@PathVariable("id") int id, RentalDto rentalDto, BindingResult bindingResult, Model model){
        if (bindingResult.hasErrors()){
            rentalDto.setId(id);
            return "editRental";
        }
        Rental rental = rentalMapper.mapToEntity(rentalDto);
        rentalRepository.save(rental);
        return "redirect:/rental/";
    }
    @GetMapping("/delete/{id}")
    public String deleteRental(@PathVariable("id") int id){
        rentalRepository.deleteById(id);
        return "redirect:/rental/";
    }

}
