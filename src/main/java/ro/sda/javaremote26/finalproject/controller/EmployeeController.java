package ro.sda.javaremote26.finalproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ro.sda.javaremote26.finalproject.dto.CarDto;
import ro.sda.javaremote26.finalproject.dto.EmployeeDto;
import ro.sda.javaremote26.finalproject.entities.Car;
import ro.sda.javaremote26.finalproject.entities.Employee;
import ro.sda.javaremote26.finalproject.enums.Brand;
import ro.sda.javaremote26.finalproject.enums.Position;
import ro.sda.javaremote26.finalproject.enums.Status;
import ro.sda.javaremote26.finalproject.mapper.EmployeeMapper;
import ro.sda.javaremote26.finalproject.repository.BranchRepository;
import ro.sda.javaremote26.finalproject.repository.EmployeeRepository;
import ro.sda.javaremote26.finalproject.utils.ImageUtil;

import java.io.IOException;
import java.util.Optional;

@Controller
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    EmployeeMapper employeeMapper;
    @Autowired
    BranchRepository branchRepository;

    @GetMapping("/")
    public String showAllEmployees(Model model) {
        model.addAttribute("employeeList", employeeRepository.findAll());
        model.addAttribute("imgUtil", new ImageUtil());
        return "employeeList";
    }

    @GetMapping("/add")
    public String showAddForm(Model model){
        EmployeeDto employeeDto = new EmployeeDto();
        model.addAttribute("employeeDto",employeeDto);
        model.addAttribute("availableBranches", branchRepository.findAll());
        model.addAttribute("positionValues", Position.values());
        return "addEmployee";
    }

    @PostMapping("/add")
    public String addEmployee(@RequestParam("file") MultipartFile file, EmployeeDto employeeDto, BindingResult bindingResult, Model model){
        if (bindingResult.hasErrors()){
            return "addEmployee";
        }
        Employee employee = employeeMapper.mapToEntity(employeeDto);
        employee.setImage(ImageUtil.resizeAndCrop(file, 150, 150));
        employeeRepository.save(employee);
        return "redirect:/employee/";
    }

    @GetMapping("/edit/{id}")
    public String showEditForm(@PathVariable("id") int id, Model model){
        Optional<Employee> result = employeeRepository.findById(id);
        if (result.isEmpty()){
            throw new RuntimeException("Employee not found");
        }else {
            EmployeeDto employeeDto= employeeMapper.mapToDto(result.get());
            model.addAttribute("employeeDto", employeeDto);
        }
        model.addAttribute("availableBranches", branchRepository.findAll());
        model.addAttribute("positionValues", Position.values());
        return "editEmployee";
    }
    @PostMapping("/edit/{id}")
    public String editEmployee(@PathVariable("id") int id, @RequestParam("file") MultipartFile file, EmployeeDto employeeDto, BindingResult bindingResult, Model model){
        if (bindingResult.hasErrors()){
            employeeDto.setId(id);
            return "editEmployee";
        }
        Employee employee = employeeMapper.mapToEntity(employeeDto);
        employee.setImage(ImageUtil.resizeAndCrop(file, 150, 150));
        employeeRepository.save(employee);
        return "redirect:/employee/";
    }
    @GetMapping("/delete/{id}")
    public String deleteEmployee(@PathVariable("id") int id){
        employeeRepository.deleteById(id);
        return "redirect:/employee/";
    }
}
