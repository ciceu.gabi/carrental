package ro.sda.javaremote26.finalproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ro.sda.javaremote26.finalproject.dto.BranchDto;
import ro.sda.javaremote26.finalproject.dto.CarDto;
import ro.sda.javaremote26.finalproject.entities.Branch;
import ro.sda.javaremote26.finalproject.entities.Car;
import ro.sda.javaremote26.finalproject.enums.Brand;
import ro.sda.javaremote26.finalproject.enums.Status;
import ro.sda.javaremote26.finalproject.mapper.BranchMapper;
import ro.sda.javaremote26.finalproject.repository.BranchRepository;
import ro.sda.javaremote26.finalproject.repository.CarRepository;
import ro.sda.javaremote26.finalproject.repository.EmployeeRepository;
import ro.sda.javaremote26.finalproject.repository.RentalRepository;

import javax.persistence.Entity;
import java.util.Optional;

@Controller
@RequestMapping("/branch")
public class BranchController {

    @Autowired
    BranchRepository branchRepository;
    @Autowired
    BranchMapper branchMapper;
    @Autowired
    RentalRepository rentalRepository;


    @GetMapping("/")
    public String showAllBranches(Model model) {
        model.addAttribute("branchList", branchRepository.findAll());
        return "branchList";
    }

    @GetMapping("/add")
    public String showAddForm(Model model) {
        BranchDto branchDto = new BranchDto();
        model.addAttribute("branchDto", branchDto);
        model.addAttribute("availableRentals", rentalRepository.findAll());
        return "addBranch";
    }

    @PostMapping("/add")
    public String addBranch(BranchDto branchDto, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "addBranch";
        }
        Branch branch = branchMapper.mapToEntity(branchDto);
        branchRepository.save(branch);
        return "redirect:/branch/";
    }

    @GetMapping("/edit/{id}")
    public String showEditForm(@PathVariable("id") int id, Model model){
        Optional<Branch> result = branchRepository.findById(id);
        if (result.isEmpty()){
            throw new RuntimeException("Branch not found");
        }else {
            BranchDto branchDto = branchMapper.mapToDto(result.get());
            model.addAttribute("branchDto", branchDto);
        }
        model.addAttribute("availableRentals", rentalRepository.findAll());
        return "editBranch";
    }
    @PostMapping("/edit/{id}")
    public String editBranch(@PathVariable("id") int id,BranchDto branchDto, BindingResult bindingResult, Model model){
        if (bindingResult.hasErrors()){
            branchDto.setId(id);
            return "editBranch";
        }
        Branch branch = branchMapper.mapToEntity(branchDto);
        branchRepository.save(branch);
        return "redirect:/branch/";
    }
    @GetMapping("/delete/{id}")
    public String deleteBranch(@PathVariable("id") int id){
        branchRepository.deleteById(id);
        return "redirect:/branch/";
    }
}
