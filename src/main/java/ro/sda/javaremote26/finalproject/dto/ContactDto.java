package ro.sda.javaremote26.finalproject.dto;

import lombok.Data;

@Data
public class ContactDto {
    private String name;
    private String firstName;
    private String email;
    private String address;
    private String message;

}
