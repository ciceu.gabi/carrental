package ro.sda.javaremote26.finalproject.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class RentalDto {

    private Integer id;
    private String name;
    private String internetDomain;
    private String contactAddress;
    private Integer ownerId;
    private String logoType;
}
