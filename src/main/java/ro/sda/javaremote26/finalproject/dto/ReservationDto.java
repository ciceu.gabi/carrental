package ro.sda.javaremote26.finalproject.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import ro.sda.javaremote26.finalproject.entities.Branch;

import java.util.Date;

@NoArgsConstructor
@Data
public class ReservationDto {
    private Integer id;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE, fallbackPatterns = { "M/d/yy", "dd.MM.yyyy" ,"yyyy-mm-dd"})
    private Date dateOfBooking;
    private Integer carId;
    private String carName;
    private Long customerId;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE, fallbackPatterns = { "M/d/yy", "dd.MM.yyyy","yyyy-mm-dd" })
    private Date dateFrom;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE, fallbackPatterns = { "M/d/yy", "dd.MM.yyyy","yyyy-mm-dd" })
    private Date dateTo;
    private int amount;
    private Integer branchId;
    private String branchName;
    private Integer returnDepartmentId;
    private String returnDepartmentName;
    private String client;
}
