package ro.sda.javaremote26.finalproject.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import ro.sda.javaremote26.finalproject.entities.Employee;
import ro.sda.javaremote26.finalproject.entities.Reservation;

import java.util.Date;

@NoArgsConstructor
@Data
public class RefundDto {
    private Integer id;
    private Integer employee;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE, fallbackPatterns = { "M/d/yy", "dd.MM.yyyy" ,"yyyy-mm-dd"})
    private Date dateOfReturn;
    private Integer reservation;
    private int surcharge;
    private String comments;
}
