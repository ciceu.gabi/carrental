package ro.sda.javaremote26.finalproject.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@NoArgsConstructor
@Data
public class LoanDto {

    private Integer id;
    private Integer employee;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE, fallbackPatterns = { "M/d/yy", "dd.MM.yyyy" ,"yyyy-mm-dd"})
    private Date dateOfRental;
    private Integer reservation;
    private String comments;
}
