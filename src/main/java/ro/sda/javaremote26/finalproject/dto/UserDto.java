package ro.sda.javaremote26.finalproject.dto;

import lombok.Data;
import ro.sda.javaremote26.finalproject.entities.Role;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class UserDto {
    private Long id;
    private String name;
    private String firstName;
    private String email;
    private String address;
    private String password;
    private String country;
    private boolean admin;
    private List<Role> availableRoles;
    private Integer roleId;


}
