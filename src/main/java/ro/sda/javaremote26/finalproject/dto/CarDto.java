package ro.sda.javaremote26.finalproject.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ro.sda.javaremote26.finalproject.enums.Brand;
import ro.sda.javaremote26.finalproject.enums.Status;

import java.io.File;

@NoArgsConstructor
@Getter
@Setter
public class CarDto {
    private Integer id;
    private Brand brand;
    private String model;
    private String bodyType;
    private int year;
    private String color;
    private double kilometers;
    private Status status;
    private int amountPerDay;
    private BranchDto branch;
    private Integer branchId;
    private String description;
    private String image;
}
