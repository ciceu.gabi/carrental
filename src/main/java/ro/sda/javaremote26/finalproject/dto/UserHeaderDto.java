package ro.sda.javaremote26.finalproject.dto;

import lombok.Data;

@Data
public class UserHeaderDto {

    private String name;
}
