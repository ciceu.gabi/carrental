package ro.sda.javaremote26.finalproject.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import ro.sda.javaremote26.finalproject.entities.Rental;

@NoArgsConstructor
@Data
public class BranchDto {
    private Integer id;
    private String address;
    private String branchName;
    private Integer rentalId;

    @Override
    public String toString() {
        return "BranchDto{" +
                "branchName='" + branchName + '\'' +
                '}';
    }
}
